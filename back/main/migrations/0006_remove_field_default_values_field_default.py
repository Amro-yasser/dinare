# Generated by Django 4.1.1 on 2022-12-18 14:06

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_field_default_values'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='field',
            name='default_values',
        ),
        migrations.AddField(
            model_name='field',
            name='default',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=200), blank=True, null=True, size=None),
        ),
    ]
