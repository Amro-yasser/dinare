# Generated by Django 4.1.1 on 2022-12-18 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_alter_category_parent'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='default_values',
            field=models.JSONField(default=[{}]),
        ),
    ]
