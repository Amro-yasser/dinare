from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
import json
from .models import *
from .serializers import *


class FieldList(generics.ListCreateAPIView):
  queryset = Field.objects.all()
  serializer_class = FieldSerializer

class FormList(generics.ListCreateAPIView):
  queryset = Form.objects.all()
  serializer_class = FormSerializer

 


class FormDetail(generics.RetrieveUpdateDestroyAPIView):
  lookup_field = 'pk'
  queryset = Form.objects.all()
  serializer_class = FormSerializer

  def get_queryset(self):
    
    return super().get_queryset()



class CategoryList(generics.ListCreateAPIView):
  queryset = Category.objects.filter(level=1)
  serializer_class = CategorySerializer

class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
  lookup_field = 'pk'
  queryset = Category.objects.all()
  serializer_class = CategorySerializer


  def get(self, request, *args, **kwargs):
    
    result =  self.retrieve(request, *args, **kwargs)
    
    childrens = self.get_queryset().get(pk=self.kwargs.get('pk',None)).children.all()
    serializer = self.get_serializer(childrens,many=True)
    res = json.loads(json.dumps(serializer.data))
    return Response({'childrens': res,**result.data})

  # def destroy(self,request, *args, **kwargs):
  #   category = self.kwargs.get('pk', None)
  #   print('CATEGORY: ',category)
  #   print(request.data)
  #   print(Category.objects.get(id=category).delete())
  #   return Response({'message':"Category Deleted Successfully"})

