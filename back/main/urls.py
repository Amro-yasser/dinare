from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings




urlpatterns = [
    path("api/field/", FieldList.as_view(), name="fields"),

    path("api/form/", FormList.as_view(), name="forms"),
    path('api/form/<int:pk>', FormDetail.as_view(), name='update_form'),

    path("api/category/", CategoryList.as_view(), name="categories"),
    path('api/category/<int:pk>', CategoryDetail.as_view(), name='update_category'),
    
]
urlpatterns.extend(static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))