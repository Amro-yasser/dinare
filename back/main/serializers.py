from .models import *
from rest_framework import serializers

class FieldSerializer(serializers.ModelSerializer):
  default = serializers.ListField(child=serializers.CharField())
  class Meta:
    model = Field
    fields = ['name','type','default']
    
  
class CategorySerializer(serializers.ModelSerializer):
  # fields = FieldSerializer(many=True,required=False)
  fields = serializers.SlugRelatedField(many=True, read_only=False, slug_field='name',queryset =Field.objects.all() )

  class Meta:
    model = Category
    fields = ['parent','title','fields']

  
  def create(self,validated_data):

    parent = validated_data.get('parent',None) 

    if parent:
      for field in parent.fields.all():
        validated_data['fields'].append(field)

    return super(CategorySerializer, self).create(validated_data)

  def update(self,instance,validated_data):

    fields = validated_data.get('fields',None)
    childrens = instance.children.all()
    old_parent_fields = instance.fields.all()

    if childrens:
      for child in childrens:
        fields_to_remove = child.fields.filter(id__in=old_parent_fields)
        
        for field in fields_to_remove:
          child.fields.remove(field.id)

        for field in fields:

          child.fields.add(field.id) 
          
    return super(CategorySerializer,self).update(instance,validated_data)
  

class FormSerializer(serializers.ModelSerializer):
  category = serializers.SlugRelatedField(many=True, read_only=False, slug_field='title',queryset =Category.objects.all() )
  class Meta:
    model = Form
    fields = '__all__'


  # def create(self, validated_data):
    
  #   return super(FormSerializer, self).create(validated_data)



