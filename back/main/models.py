from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.postgres.fields import ArrayField

# Create your models here.

class Field(models.Model):
  class Meta:
    db_table = 'field'

  name = models.CharField(max_length=255)
  type = models.CharField(max_length=255)
  default = ArrayField(models.CharField(max_length=200), blank=True, null=True)

  def __str__(self) :
      return self.name

class Category(MPTTModel):
  class Meta:
    db_table = 'category'

  class MPTTMeta:
        order_insertion_by = ['title']

  parent = TreeForeignKey('Category',blank=True, null=True, related_name='children',on_delete=models.CASCADE)
  title = models.CharField(max_length=255,unique=True)
  fields = models.ManyToManyField(Field,blank=True, null=True)

  def __str__(self) :
    return self.title


class Form(models.Model):
  class Meta:
    db_table = 'form'
  category = models.ForeignKey(Category,on_delete=models.CASCADE)
  data = models.JSONField()  

  def __str__(self) :
    return self.category.title
