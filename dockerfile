FROM python:3.10.1
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code

RUN pip install --upgrade pip

ADD requirements.txt /code/
RUN pip install -r requirements.txt --default-timeout=8000
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install nodejs -y
# RUN npm i -g npm@latest 
ADD ./front/package.json /code/front/
WORKDIR /code/front
# ADD package.json /code/
RUN npm install
WORKDIR /code
ADD . /code/

RUN apt-get update
