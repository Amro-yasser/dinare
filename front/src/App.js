import './App.css';
import { useState, useEffect } from 'react'
import axios from 'axios';


function App() {
  const [categories,setCategories] = useState([])

  useEffect(()=> {
    axios.get('http://127.0.0.1:8000/api/category/')
    .then((res)=>{
      setCategories(res.data)
    })

  },[])

  const displayChildrens = () =>{

  }
  return (
    <div className="App">
      <h1>Dinare</h1>
      { 
      categories.map((item)=>{
       return (

        <h1>{item.title}</h1>

      );})
     }
    </div>
  );
}

export default App;
